FROM node:20.2-alpine

ENV TZ=Europe/Zurich

# Add composer to image
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Add PHP + exts
RUN apk update && apk add --no-cache \
    shadow \
    curl \
    fcgi \
    busybox \
    php82 \
    php82-fpm \
    php82-cli \
    php82-gd \
    php82-ctype \
    php82-curl \
    php82-dom \
    php82-iconv \
    php82-json \
    php82-xml \
    php82-mbstring \
    php82-openssl \
    php82-simplexml \
    php82-opcache \
    php82-pecl-memcached \
    php82-pecl-igbinary \
    php82-exif \
    php82-fileinfo \
    php82-intl \
    php82-zip \
    php82-session \
    php82-tokenizer \
    php82-sockets \
    php82-pcntl \
    php82-ffi \
    php82-phar \
    icu-data-full \
    git

RUN ln -sf /usr/bin/php82 /usr/bin/php

USER node

# Add kirby cli and make health command exec
RUN composer global require getkirby/cli
RUN chmod 777 ~/.composer/vendor/bin/kirby
RUN ln -s ~/.composer/vendor/bin/kirby /usr/local/bin/kirby

WORKDIR /var/www/html

EXPOSE 8888

CMD yarn && yarn dev
